using MW.JMAYTA.Model.Entities;

namespace MW.JMAYTA.Web.Models;

public class SocialNetworkViewModel : SocialNetwork
{
    public SocialNetworkViewModel() { }

    public SocialNetworkViewModel(SocialNetwork socialNetwork)
    {
        SocialNetworkId = socialNetwork.SocialNetworkId;
        Name = socialNetwork.Name;
        AccountUrl = socialNetwork.AccountUrl;
        IconUrl = socialNetwork.IconUrl;
        Icon = socialNetwork.Icon;
        Visible = socialNetwork.Visible;
    }
}
