using MW.JMAYTA.Model.Entities;

namespace MW.JMAYTA.Web.Models
{
    public class PersonViewModel : Person
    {
        #region Constructor
        /// <summary>
        /// Creates an instance of <see cref="PersonViewModel"/>
        /// </summary>
        public PersonViewModel() { }

        /// <summary>
        /// Creates an instance of <see cref="PersonViewModel"/> from a 
        /// <seealso cref="Person"/> object
        /// </summary>
        /// <param name="person"><see cref="PersonViewModel"/> object</param>
        public PersonViewModel(Person? person)
        {
            PersonId = person?.PersonId;
            FirstName = person?.FirstName;
            LastName = person?.LastName;
            EmailAddress = person?.EmailAddress;
            ExperienceViewModel = person?.Experience?.Select(
                job => new JobViewModel(job)
            );
            SocialNetworksViewModel =
                person?
                    .SocialNetworks?
                    .Where(
                        socialNetwork =>
                            socialNetwork.Visible
                    ).Select(
                        socialNetwork =>
                            new SocialNetworkViewModel(socialNetwork)
                    );
            AvatarUrl = person?.AvatarUrl;
            Profile = person?.Profile;
        }
        #endregion

        public IEnumerable<JobViewModel>? ExperienceViewModel { get; set; }
        public IEnumerable<SocialNetworkViewModel>? SocialNetworksViewModel { get; set; }
        public string? FullName
        {
            get
            {
                string? first = FirstName?.Split(" ").FirstOrDefault();
                string? last = LastName?.Split(" ").FirstOrDefault();
                return string.Concat(first, " ", last).Trim();
            }
        }
    }
}
