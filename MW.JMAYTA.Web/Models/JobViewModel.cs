using MW.JMAYTA.Model.Entities;
using System.Globalization;

namespace MW.JMAYTA.Web.Models;

public class JobViewModel : Job
{
    #region Constructor
    /// <summary>
    /// Initialize a new instance for <see cref="JobViewModel"/>
    /// </summary>
    public JobViewModel() { }

    /// <summary>
    /// Initialize a new instance for <see cref="JobViewModel"/>
    /// </summary>
    /// <param name="job"><see cref="Job"/> object</param>
    public JobViewModel(Job? job)
    {
        JobId = job?.JobId;
        Title = job?.Title;
        Description = job?.Description;
        StartDate = job?.StartDate;
        EndDate = job?.EndDate;
        Company = job?.Company;
        Person = job?.Person;
        Tags = job?.Tags;
    }
    #endregion

    public string? StartDateString
    {
        get => StartDate?.ToString(
            "MMMM yyyy", new CultureInfo("es-ES")
        );
    }
    public string? EndDateString
    {
        get => EndDate?.ToString(
                "MMMM yyyy", new CultureInfo("es-ES")
            ) ?? "actualidad";
    }
}
