/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["**/*.cshtml"],
    darkMode: "class",
    theme: {
        extend: {},
        fontFamily: {
            "serif": ["Alegreya", "serif"],
            "sans": ["Poppins", "sans-serif"],
            "mono": ["JetBrains\\ Mono", "mono"],
            "display": ["Press\\ Start\\ 2P"],
            "condensed": ["Fira\\ Sans\\ Condensed"]
        },
    },
    plugins: [],
};
