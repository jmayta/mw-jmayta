using Azure.Extensions.AspNetCore.Configuration.Secrets;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using MW.JMAYTA.Repository;
using MW.JMAYTA.Repository.Contracts;
using MW.JMAYTA.Service;
using MW.JMAYTA.Service.Contracts;
using System.Net.Http.Headers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRouting(options => options.LowercaseUrls = true);
builder.Services.AddControllersWithViews();

// In Production
if (builder.Environment.IsProduction())
{
    // Add Secrets from AzureKeyVault to Secrets
    var secretClient = new SecretClient(
        new Uri(
            builder.Configuration["KeyVaultConfiguration:KeyVaultURL"] ?? ""
        ),
        new DefaultAzureCredential()
    );
    builder.Configuration.AddAzureKeyVault(secretClient, new KeyVaultSecretManager());
}

builder.Services.AddHttpClient("Gitlab", client =>
{
    // GitLab API Base URL
    client.BaseAddress = new Uri(
        builder.Configuration.GetValue<string>("Gitlab:Url") ?? ""
    );
    // GitLab API Authorization
    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
        "Bearer",
        builder.Configuration.GetValue<string>("Gitlab:Key")
    );
});

// Inject services to application
builder.Services
    .AddSingleton<IConfiguration>(builder.Configuration)
    .AddSingleton<IApiClientFactory, GitLabService>()
    .AddScoped<IPersonRepository, PersonRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for 
    // production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
