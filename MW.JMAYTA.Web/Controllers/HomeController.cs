﻿using Microsoft.AspNetCore.Mvc;
using MW.JMAYTA.Model.Entities;
using MW.JMAYTA.Repository.Contracts;
using MW.JMAYTA.Web.Models;
using System.Diagnostics;

namespace MW.JMAYTA.Web.Controllers;

public class HomeController : Controller
{
    private readonly IPersonRepository _personRepository;

    public HomeController(
        IPersonRepository personRepository
    )
    {
        _personRepository = personRepository;
    }

    public async Task<IActionResult>? Index()
    {
        PersonViewModel? personViewModel = null;
        Person? personRetrieved =
            await _personRepository.GetPersonAsync("46594611")!;
        if (personRetrieved != null)
        {
            personViewModel = new PersonViewModel(personRetrieved);
        }
        return View(model: personViewModel);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(
            new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            }
        );
    }
}
