
namespace MW.JMAYTA.Model.Entities;
public class ApiConfiguration
{
    public string? BaseUrl { get; set; }
    public string? Source { get; set; }
    public string? Token { get; set; }
}
