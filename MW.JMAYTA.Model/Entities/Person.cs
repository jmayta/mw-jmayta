using System.Text.RegularExpressions;

namespace MW.JMAYTA.Model.Entities
{
    [Serializable]
    public class Person
    {
        public string? PersonId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? EmailAddress { get; set; }
        public string? AvatarUrl { get; set; }
        public IEnumerable<Job>? Experience { get; set; }
        public IEnumerable<SocialNetwork>? SocialNetworks { get; set; }
        public Profile? Profile { get; set; } // Presentation, Bio, BioImageUrl, SocialNetworks
    }
}
