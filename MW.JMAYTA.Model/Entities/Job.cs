namespace MW.JMAYTA.Model.Entities
{
    [Serializable]
    public class Job
    {
        public int? JobId { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Company? Company { get; set; }
        public Person? Person { get; set; }
        public IEnumerable<string>? Tags { get; set; }
    }
}
