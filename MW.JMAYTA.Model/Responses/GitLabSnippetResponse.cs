using System.Text.Json.Serialization;

namespace MW.JMAYTA.Model.Responses;
[Serializable]
public class GitLabSnippetResponse
{
    public int? Id { get; set; }
    public string? Title { get; set; }
    [JsonPropertyName("file_name")]
    public string? FileName { get; set; }
    public string? Description { get; set; }
    public string? Visibility { get; set; }
    [JsonPropertyName("author")]
    public GitLabAuthorResponse? Author { get; set; }
    [JsonPropertyName("expires_at")]
    public DateTime? ExpiresAt { get; set; }
    [JsonPropertyName("updated_at")]
    public DateTime? UpdatedAt { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }
    [JsonPropertyName("project_id")]
    public int? ProjectId { get; set; }
    [JsonPropertyName("web_url")]
    public string? WebUrl { get; set; }
    [JsonPropertyName("raw_url")]
    public string? RawUrl { get; set; }
}
