using System.Text.Json.Serialization;

namespace MW.JMAYTA.Model.Responses;
[Serializable]
public class GitLabAuthorResponse
{
    public int? Id { get; set; }
    public string? Username { get; set; }
    public string? Email { get; set; }
    public string? Name { get; set; }
    public string? State { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }
}
