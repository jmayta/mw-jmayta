using Microsoft.Extensions.Configuration;
using MW.JMAYTA.Service.Contracts;

namespace MW.JMAYTA.Service;

public class GitLabService : IApiClientFactory
{
    #region Properties & Variables
    // Private
    private bool disposed = false;
    // Private ReadOnly
    private readonly IHttpClientFactory _httpClientFactory;
    // Public
    #endregion

    #region Dispose support
    protected virtual void Dispose(bool disposing)
    {
        if (!disposed && disposing)
        {
            disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~GitLabService()
    {
        Dispose(false);
    }
    #endregion

    #region Constructor
    public GitLabService(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }
    #endregion

    #region Methods
    public HttpClient ClientInstance()
    {
        HttpClient client = _httpClientFactory.CreateClient("Gitlab");
        return client;
    }
    #endregion


}
