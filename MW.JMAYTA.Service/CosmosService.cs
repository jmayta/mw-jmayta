﻿using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using MW.JMAYTA.Model.Entities;
using MW.JMAYTA.Service.Contracts;

namespace MW.JMAYTA.Service;


public class CosmosService
{
    #region Private variables
    private CosmosClient _client;
    private readonly IConfiguration _configuration;
    #endregion

    #region Public variables
    public CosmosClient Client
    {
        get
        {
            return _client;
        }
        set
        {
            _client = value;
        }
    }
    #endregion

    #region Disposing support
    private bool disposed = false;

    private void Dispose(bool disposing)
    {
        if (!disposed)
        {
            if (disposing)
            {
                // manage handled sources
                disposed = true;
            }
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion

    #region Constructor
    public CosmosService(IConfiguration configuration)
    {
        _configuration = configuration;

        _client = new CosmosClient(
            accountEndpoint: _configuration["CosmosDB:endpoint"],
            authKeyOrResourceToken: _configuration["CosmosDB:key"]
        );
    }
    #endregion

    #region Methods
    public object ClientInstance()
    {
        return _client;
    }

    public async Task GetDatabase()
    {
        Database database = await _client
            .CreateDatabaseIfNotExistsAsync(
                id: "munayworkers"
            );
        Console.WriteLine($"Database:\t{database.Id}");

        Container container = await database.CreateContainerIfNotExistsAsync(
            id: "persons",
            partitionKeyPath: "/PersonId",
            throughput: 400
        );
        Console.WriteLine($"Container:\t{container.Id}");

        Person person = new Person
        {
            // id = Guid.NewGuid().ToString(),
            PersonId = "46594611",
            FirstName = "Jheison"
        };

        Person createdPerson = await container.CreateItemAsync<Person>(
            item: person,
            partitionKey: new PartitionKey(person.PersonId)
        );
        Console.WriteLine($"Created person:\t{createdPerson.PersonId}\t[{createdPerson.FirstName}]");


        Person readPerson = await container.ReadItemAsync<Person>(
             id: person.PersonId,
            partitionKey: new PartitionKey(person.PersonId)
        );
        Console.WriteLine($"Person retrieved:\t{readPerson.FirstName}");

        // Create query using a SQL string and parameters
        var query = new QueryDefinition(
            query: "SELECT * FROM persons p WHERE p.PersonId = @personId"
        ).WithParameter("@personId", "46594611");

        FeedIterator<Person> feed = container.GetItemQueryIterator<Person>(
            queryDefinition: query
        );

        while (feed.HasMoreResults)
        {
            FeedResponse<Person> response = await feed.ReadNextAsync();
            foreach (Person p in response)
            {
                Console.WriteLine($"Found item:\t{p.PersonId}");
            }
        }
    }

    // TODO: Verificar si la base de datos existe
    // TODO: Verificar si el contenedor existe
    // TODO: Verificar si el contenedor está vacío
    // TODO: Rellenar el contenedor con información base
    #endregion
}
