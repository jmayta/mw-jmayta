namespace MW.JMAYTA.Service.Contracts;

public interface IApiClientFactory : IDisposable
{
    /// <summary>
    /// Returns a HttpClient instance
    /// </summary>
    /// <returns>HttpClient</returns>
    HttpClient ClientInstance();
}
