using System.Net.Http.Headers;
using Microsoft.Azure.Cosmos.Serialization.HybridRow.Schemas;
using MW.JMAYTA.Service.Contracts;

namespace MW.JMAYTA.Service;

public class ApiClientFactory : IApiClientFactory
{
    #region Properties & Variables
    // Private
    private bool _disposed = false;
    // Private ReadOnly
    private readonly IHttpClientFactory _httpClientFactory;
    #endregion

    #region Constructor
    /// <summary>
    /// Returns an instace of ApiClientFactory
    /// </summary>
    /// <param name="httpClientFactory"></param>
    /// <param name="apiName"></param>
    public ApiClientFactory(
        IHttpClientFactory httpClientFactory
        )
    {
        _httpClientFactory = httpClientFactory;
    }
    #endregion

    #region Methods
    /// <summary>
    /// Returns a HttpClient
    /// </summary>
    /// <returns></returns>
    public HttpClient ClientInstance()
    {
        return _httpClientFactory.CreateClient();
    }
    #endregion

    #region Dispose Support
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (!_disposed && disposing)
        {
            // manage unhandled sources
            _disposed = true;
        }
    }
    #endregion
}
