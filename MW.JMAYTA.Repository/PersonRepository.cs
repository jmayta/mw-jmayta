using Microsoft.Extensions.Configuration;
using MW.JMAYTA.Model.Entities;
using MW.JMAYTA.Model.Responses;
using MW.JMAYTA.Repository.Contracts;
using MW.JMAYTA.Service.Contracts;
using System.Net.Http.Json;


namespace MW.JMAYTA.Repository;

public class PersonRepository : IPersonRepository
{
    private readonly IApiClientFactory _clientFactory;

    public PersonRepository(
        IApiClientFactory clientFactory
        )
    {
        _clientFactory = clientFactory;
    }

    public async Task<IEnumerable<Person?>?> GetPersons()
    {
        IEnumerable<Person?>? persons = null;
        using HttpClient client = _clientFactory.ClientInstance();
        var response = await client.GetFromJsonAsync<GitLabSnippetResponse>(
            "snippets/2582002"
            );
        if (response != null)
        {
            persons = await client.GetFromJsonAsync<IEnumerable<Person?>>(
                "snippets/2582002/raw"
                );
        }
        persons ??= new List<Person>();

        return persons.AsEnumerable();
    }

    public async Task<Person?> GetPersonAsync(string personId)
    {
        Person? personRetrieved = null;
        IEnumerable<Person?>? persons = await GetPersons();
        personRetrieved = persons?.FirstOrDefault(
            person => person?.PersonId == personId
            );

        return personRetrieved;
    }
}
