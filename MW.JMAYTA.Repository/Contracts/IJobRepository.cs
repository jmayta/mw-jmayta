using MW.JMAYTA.Model.Entities;

namespace MW.JMAYTA.Repository.Contracts;
public interface IJobRepository
{
    Job GetJobById(string jobId);
    IEnumerable<Job> GetJobs();
}
