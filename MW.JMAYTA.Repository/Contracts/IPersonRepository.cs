using MW.JMAYTA.Model.Entities;

namespace MW.JMAYTA.Repository.Contracts;

public interface IPersonRepository
{
    Task<IEnumerable<Person?>?> GetPersons();
    Task<Person?>? GetPersonAsync(string personId);
}
